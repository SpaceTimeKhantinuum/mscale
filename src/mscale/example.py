def example_function(x: int):
    """
    Returns the input int.

    This function just returns the input int.

    Parameters
    ----------
    arg1 : int
        Any integer.

    Returns
    -------
    int
        The same input

    """
    return x
