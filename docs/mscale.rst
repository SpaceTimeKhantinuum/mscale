mscale package
==============

Submodules
----------

mscale.activations module
-------------------------

.. automodule:: mscale.activations
   :members:
   :undoc-members:
   :show-inheritance:

mscale.example module
---------------------

.. automodule:: mscale.example
   :members:
   :undoc-members:
   :show-inheritance:

mscale.layers module
--------------------

.. automodule:: mscale.layers
   :members:
   :undoc-members:
   :show-inheritance:

mscale.mscalev5 module
----------------------

.. automodule:: mscale.mscalev5
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: mscale
   :members:
   :undoc-members:
   :show-inheritance:
