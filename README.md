# mscale

Implementation of multi-scale (mscale) neural network layers.

[Documentation](https://mscale.readthedocs.io/en/latest/index.html)


